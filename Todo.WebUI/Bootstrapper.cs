using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using Todo.Repository;
using Todo.WebUI.Code.Managers;

namespace Todo.WebUI
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
      var container = BuildUnityContainer();

      DependencyResolver.SetResolver(new UnityDependencyResolver(container));

      return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();
        //sql connection
      //string connectionString = ;
      //container.RegisterType<ITaksRepository, FakeTaksRepository>(new InjectionConstructor(connectionString));
      container.RegisterType<ITaskRepository, FakeTaskRepository>();
      container.RegisterType<IUserRepository, FakeUserRepository>();
      container.RegisterType<ISecurityManager, FormsSecurityManager>();
      RegisterTypes(container);

      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
    
    }
  }
}