﻿function validate() {
    var input = document.getElementById('text');
    var error = document.getElementById('error');
    if (input.value.trim().length < 1) {
        error.style.display = 'block';
        error.innerHTML = "Title cannot be empty";
        return false;
    }
}

(function () {
    $(document).ready(function () {
        init();
        $("tr").each(function () {
            if ($('input[type="checkbox"]').prop('checked')) {
                $("tr:eq(3) td:eq(1)").css("color", "blue");
                $("td:eq(1)").css("text-decoration", "line-through");
            }
        });
    });

    var onTaskStatusFilterChanged = function (e) {
        var status = e.target.value;
        console.log(status);
        console.log("onTaskStatusFilterChanged");
        $.ajax({
            type: "post",
            dataType: "html",
            url: "/task/TasksPatrial",
            data: {
                taskStatus: status
            }
        }).done(function (data) {
            $("#tasks-table").html(data);
            console.log("AJAX FILTER Done");
        }).fail(function(){
            console.log("AJAX FILTER Fail");
        });
    };

    var onIsDoneClick = function (e) {
        var ch = e.target;
        var isDone = ch.checked;
        var taskId = ch.getAttribute("data-task-id");
        //console.log("[tasks-page]-checkbox click", isDone, ch.dataset.taskId);
        //console.log("[tasks-page]-checkbox click", isDone, ch.getAttribute("data-task-id"));
            $.ajax({
                url: "/Task/ChangeStatus",
                type: "POST",
                dataType: "text",
                data: {
                    taskId: taskId,
                    isDone: isDone
                }
            }).done(function () {
                console.log("done ajax");
                if (isDone) {
                    $("tr:eq(" + taskId + ")").css("color", "blue");
                    $("td:eq(" + taskId + ")").css("text-decoration", "line-through");
                }
                else {
                    $("tr").css("color", "green");
                    $("td").css("text-decoration", "none");
                }
            }).fail(function () { console.log("fail ajax"); });
        };

        var init = function () {
            $('input[type="checkbox"]').on("click", onIsDoneClick);
            $("#task-status-filter").on("change", onTaskStatusFilterChanged);
        };
})();
