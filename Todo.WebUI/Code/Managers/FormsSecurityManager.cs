﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Todo.Repository;

namespace Todo.WebUI.Code.Managers
{
    public class FormsSecurityManager : ISecurityManager
    {
        private readonly IUserRepository _userRepository;

        public FormsSecurityManager(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public bool Login(string userName, string password)
        {
            if (_userRepository.IsUser(userName, password))
            {
                FormsAuthentication.SetAuthCookie(userName, false);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}