﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Todo.WebUI.Code.Managers
{
    public interface ISecurityManager
    {
        bool Login(string userName, string password);
        void Logout();
    }
}