﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.WebUI.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsDone { get; set; }

        public TaskModel(int id, string title, bool isdone)
        {
            Id = id;
            Title = title;
            IsDone = isdone;
        }
    }
}
