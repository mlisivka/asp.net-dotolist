﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Repository;
using Todo.Entities;
using Todo.WebUI.Models;

namespace Todo.WebUI.Controllers
{
    [Authorize]
    public class TaskController : Controller
    {
        private readonly ITaskRepository _taskRepository;

        public TaskController()
        {
            this._taskRepository = new FakeTaskRepository();
        }

        public TaskController(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Task = _taskRepository.GetAll();
            return View();
        }

        [HttpPost]
        public ActionResult Index(string newTask)
        {
            if (string.IsNullOrWhiteSpace(newTask))
            {
                ViewBag.Error = "Title cannot be empty";
            }
            else if (newTask.Length > 50)
            {
                ViewBag.Error = "Title cannot be more 50 symbols";
            }
            else
            {
                _taskRepository.Add(newTask);
                ViewBag.Title = newTask;
            }
            ViewBag.Task = this._taskRepository.GetAll();
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int taskId)
        {
            this._taskRepository.Remove(taskId);
            return RedirectToAction("Index", "Task");
        }

        /*[HttpPost]
        public ActionResult Done(int taskId)
        {
            this._taskRepository.Update(taskId);
            return RedirectToAction("Index", "Task");
        }*/

        [HttpPost]
        public ActionResult ChangeStatus(int taskId, bool isDone)
        {
            TaskEntity entity = _taskRepository.GetById(taskId);
            TaskModel model = new TaskModel(entity.Id, entity.Title, isDone);
            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            TaskEntity entity = _taskRepository.GetById(id);
            TaskModel model = new TaskModel(entity.Id, entity.Title, entity.IsDone);
            return View(model);
        }

        [HttpPost]
        public ActionResult TasksPatrial(string taskStatus)
        {
            List<TaskEntity> tasks;
            if (taskStatus == "true")
                tasks = _taskRepository.GetAll().Where(r => r.IsDone == true).ToList();
            else if (taskStatus == "false")
                tasks = _taskRepository.GetAll().Where(r => r.IsDone == false).ToList();
            else
                tasks = _taskRepository.GetAll();

            return PartialView("TasksPartial", tasks);
        }
    }
}
