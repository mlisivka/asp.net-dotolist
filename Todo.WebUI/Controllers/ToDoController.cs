﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Repository;
using Todo.Entities;

namespace Todo.WebUI.Controllers
{
    public class ToDoController : Controller
    {
        private ITaskRepository _taskRepository;

        public ToDoController()
        {
            this._taskRepository = new FakeTaskRepository();
        }

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.task = _taskRepository.GetAll();
            return View();
        }

        [HttpPost]
        public ActionResult Index(string newTask)
        {
            _taskRepository.Add(newTask);
            ViewBag.task = this._taskRepository.GetAll();
            return View();
        }

        [HttpPost]
        public ActionResult Done(int id)
        {
            return View();
        }
    }
}
