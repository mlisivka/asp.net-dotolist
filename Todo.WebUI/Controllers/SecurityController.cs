﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.WebUI.Models;
using Todo.Entities;
using Todo.Repository;
using Todo.WebUI.Code.Managers;

namespace Todo.WebUI.Controllers
{
    public class SecurityController : Controller
    {
        private readonly ISecurityManager _securityRepository;

        public SecurityController(ISecurityManager securityRepository)
        {
            _securityRepository = securityRepository;
        }

        //
        // GET: /Security/
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel login)
        {
            if (_securityRepository.Login(login.Name, login.Pass))
                return RedirectToAction("Index", "Task");
            else
            {
                ViewBag.LoginErr = "Incorrect user name or password";
                return View();
            }
            /*TaskEntity entity = _taskRepository.GetById(id);
            LoginModel model = new LoginModel(entity.Name, entity.Pass);
            return View(model);*/
        }

        public ActionResult Logout()
        {
            _securityRepository.Logout();
            return RedirectToAction("Index","Task");
        }
    }
}
