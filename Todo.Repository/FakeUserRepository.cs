﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;

namespace Todo.Repository
{
    public class FakeUserRepository : IUserRepository
    {
        public static List<UserEntity> users = new List<UserEntity>()
        {
            new UserEntity(1,"admin","1"),
            new UserEntity(2,"moderator","2"),
            new UserEntity(3,"redactor","3")
        };

        public bool IsUser(string name, string pass)
        {
            return users.Any(u => u.Name == name && u.Pass == pass);
        }

        public List<UserEntity> GetAll()
        {
            return users;
        }

        public UserEntity GetById(int id)
        {
            return users.SingleOrDefault(t => t.Id == id);
        }

        public List<UserEntity> Register(string username, string password)
        {
            #region Validation
            
            #endregion

            UserEntity newUser = new UserEntity(users.Count + 1, username, password);
            users.Add(newUser);
            return GetAll();
        }

        public void Remove(int id)
        {
            users.RemoveAll(t => t.Id == id);
        }
    }
}
