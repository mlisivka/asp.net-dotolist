﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;

namespace Todo.Repository
{
    public interface IUserRepository
    {
        bool IsUser(string name, string pass);
    }
}
