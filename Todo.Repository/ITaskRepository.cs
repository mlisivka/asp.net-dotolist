﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;

namespace Todo.Repository
{
    public interface ITaskRepository
    {
        List<TaskEntity> GetAll();
        TaskEntity GetById(int id);
        List<TaskEntity> Add(string Title);
        void Remove(int id);
    }
}
