﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;

namespace Todo.Repository
{
    public class FakeTaskRepository : ITaskRepository
    {
        public static List<TaskEntity> task = new List<TaskEntity>()
        {
            new TaskEntity(1,"HomeWork",true),
            new TaskEntity(2,"Go to SHAG",false),
            new TaskEntity(3,"Go home",false)
        };

        public List<TaskEntity> GetAll()
        {
            return task;
        }

        public TaskEntity GetById(int id)
        {
            return task.SingleOrDefault(t => t.Id == id);
        }

        public List<TaskEntity> Add(string Title)
        {
            #region Validation

            if (string.IsNullOrWhiteSpace(Title))
            {
                throw new ArgumentException("Title cannot be empty");
            }
            if (Title.Length > 50)
            {
                throw new ArgumentException("Title cannot be more 50 symbols");
            }

            #endregion

            TaskEntity newtask = new TaskEntity(task.Count+1,Title,false);
            task.Add(newtask);
            return GetAll();
        }

        public void Remove(int id)
        {
            task.RemoveAll(t => t.Id == id);
        }

        public List<TaskEntity> Update(int id)
        {
            /*TaskEntity copy = task.Find(t => t.Id == id);
            copy = new TaskEntity(Id, Title, true);*/
            return GetAll();
        }
    }
}
