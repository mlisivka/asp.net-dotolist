﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.Entities
{
    public class UserEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Pass { get; set; }

        public UserEntity(int id, string username, string password)
        {
            Id = id;
            Name = username;
            Pass = password;
        }
    }
}
